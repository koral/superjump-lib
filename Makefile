HEADERS = sj_heap.h sj_stack.h
CC = gcc
CFLAGS = -Wall -g

all: aslrdisable examples

examples: stack heap

%.o: %.c $(HEADERS)
	$(CC) -c -o $@ $< $(CFLAGS)

stack: stack_ex.o sj_stack.o
	$(CC) $(CFLAGS) stack_ex.o sj_stack.o -o stack

heap: heap_ex.o sj_heap.o
	$(CC) $(CFLAGS) heap_ex.o sj_heap.o -o heap

aslrdisable: aslrdisable.o
	$(CC) $(CFLAGS) -g aslrdisable.o -o aslrdisable

clean :
	-rm stack heap aslrdisable *.data *.o
