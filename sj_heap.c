#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <malloc.h>

/* store the heap */
extern int store_heap(void *heap_start)
{
        size_t sizeof_heap;
        FILE *fp;
        void *heap_end;

	/* read the current data segment end */
        heap_end = sbrk(0);
	/* calculate the size of the heap */
        sizeof_heap = heap_end - heap_start;

        printf("saving %ld bytes of heap\n", sizeof_heap);

        fp = fopen("heap.data", "w");
        fwrite(heap_start, 1, sizeof_heap, fp);

        fclose(fp);

        return 0;
}

/* load saved heap */
int load_heap(void *heap_start)
{
        size_t sizeof_heap;
        FILE *fp;
        void *fopen_heap;

        /* open the file to load back in memory */
        fp = fopen("heap.data", "r");
        /* because fopen calls malloc reread the data segment address */
        fopen_heap = sbrk(0);

        /* maesure the size of the file */
        fseek(fp, 0, SEEK_END);
        sizeof_heap = ftell(fp);
        fseek(fp, 0, SEEK_SET);
        /*allocate the memory for the heap*/
        sbrk(sizeof_heap);

        printf("loading %ld bytes of heap\n", sizeof_heap);
        /*copy back heap in memory*/
        fread(fopen_heap, 1, sizeof_heap, fp);

        fclose(fp);
        /* once that the file is closed shift the heap in the right position */
        memmove(heap_start, fopen_heap, sizeof_heap);
        /* be sure that the data segment ends at the correct address */
        sbrk(heap_start - fopen_heap);

        return 0;
}
