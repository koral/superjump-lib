#include <stdio.h>
#include <stdlib.h>

int main () {
        int a;
        int *p;

        p = malloc(sizeof(int));

        printf("Address of the main function %p\n", main);
        printf("Address of a variable allocated in the stack %p\n", &a);
        printf("Address of some memory allocated in the heap %p\n", p);

        free(p);

        return 0;
}
