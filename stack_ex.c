#include "sj_stack.h"

/* some kind of code that could be our general program to freeze and restore */
void do_something() {
	int i = 0;

	for(i = 0; i <= 10; i++) {
		printf("%d\n",i);
		if (i ==5 )
			store_stack();
	}

}

int main (int argc, char *argv[]) {

	/* some assembly inline is used: */
	/* read the actual stack address from the stack register */
	asm ("movq %%rsp, %0; "
		:"=r"(stack_start)
		);
	printf("initial stack at: %p\n", stack_start);

	if (!strcmp(argv[1], "store"))
		do_something();
	else
		load_stack();

	return 0;
}
