#include "sj_heap.h"

int main (int argc, char *argv[])
{
        void *heap_start;
        char *str;

	/* before doing every allocation we store the heap start address */
        heap_start = sbrk(0);
	printf("heap start: %p\n", heap_start);

	/* disable the use of the the memory mapping by malloc  */
	/* just in case... */
        mallopt(M_MMAP_MAX, 0);

        if(!strcmp(argv[1], "store")) {
                /* do some allocs */
                str = malloc(10);
                printf("str allocated at %p\n", str);
                strcpy(str, "foo");
                store_heap(heap_start);
        } else {
                load_heap(heap_start);
        }
        printf("heap end: %p\n", sbrk(0));

        /*read from the heap!*/
        str = (void*)0x602010;
        printf("accessing dynamic allocated string: %s\n", str);

        return 0;
}
