#include "sj_stack.h"

static size_t sizeof_stack, sizeof_reg;
static ucontext_t reg_buf;
static FILE *fp;
static int state = 0;
void *stack_start;

/* store the stack */
extern int store_stack()
{
	size_t sizeof_stack;
	void *stack_end;

	fp = fopen("stack.data", "w");
	/* read the current stack register value and store in stack_end */
	asm ("movq %%rsp, %0; "
		:"=r"(stack_end)
		);
	/* calc the actual size of the stack */
	sizeof_stack = stack_start - stack_end;
	printf("saving %ld bytes of stack\n", sizeof_stack);
	/* write the stack contenent to file */
	if (sizeof_stack != fwrite(stack_end, 1, sizeof_stack, fp)) {
		printf("error saving the stack\n");
		exit(1);
	}
	fclose(fp);
	fp = fopen("register.data", "w");
	/* use getcontext to read register values */
	getcontext(&reg_buf);
	/* because the previous is also the line from wich we will restart  */
	/* the execution we need a flag (state) to distinguish */
	/* the two behaviors */
	if (!state) {
		printf("saving %ld bytes of register values\n",
			sizeof(reg_buf));
		/* write the register content to file */
		fwrite(&reg_buf, sizeof(reg_buf), 1, fp);
		fclose(fp);
	}

	return 0;
}

/* load the stack */
extern int load_stack()
{
	size_t stack_need;
	void *stack_end;

	/* open stack.data and mesure the file size  */
	/* storing the result in sizeof_stack */
	fp =  fopen("stack.data", "r");
	fseek(fp, 0, SEEK_END);
	sizeof_stack = ftell(fp);
	fseek(fp, 0L, SEEK_SET);
	/* measure the actual stack position and calc the need of space  */
	/* for testoring the saved stack */
	asm ("movq %%rsp, %0; "
		:"=r"(stack_end)
		);
	printf("stack at: %p\n", stack_end);
	stack_need = sizeof_stack - (stack_start - stack_end);
	/* allocate the needed stack */
	alloca(stack_need);
	/* read from file and write to memory */
	if (sizeof_stack !=
		fread(stack_start - sizeof_stack, 1, sizeof_stack , fp)) {
		printf("error loading stack\n");
		exit(1);
	}
	printf("restored %ld bytes of stack from %p to %p\n", sizeof_stack,
		stack_start, (void*)(stack_start - sizeof_stack));
	fclose(fp);
	/* open the register.data file and read the size */
	fp = fopen("register.data", "r");
	fseek(fp, 0, SEEK_END);
	sizeof_reg = ftell(fp);
	fseek(fp, 0L, SEEK_SET);
	/* read the file and write in the reg_buf variable */
	if (sizeof_reg != fread(&reg_buf, 1, sizeof_reg , fp)) {
		printf("error loading register file\n");
		exit(1);
	}
	printf("register file loading done, ready to jump!\n");
	/* "Jump!" Cit. Van Halen */
	state = 1;
	setcontext(&reg_buf);
	return 0;
}
