#ifndef _sj_heap_h_included_
#define _sj_heap_h_included_

#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <malloc.h>

/* store the heap */
extern int store_heap(void *heap_start);

/* load saved heap*/
extern int load_heap(void *heap_start);

#endif
