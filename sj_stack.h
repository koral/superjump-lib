#ifndef _sj_stack_h_included_
#define _sj_stack_h_included_

#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <ucontext.h>

/* pointer to the beginning of the stack */
/* record here the position at the beginning of your main function */
extern void *stack_start;

/* store the stack */
extern int store_stack();

/* load the stack */
extern int load_stack();

#endif
