superjump
=========

This trivial library uses a couple of nice tricks to make a C program able to store to HD his state while running and then load it back also in a different run.

The only purpose for this small software is educational.

More details at: http://kkoral.blogspot.com/2014/07/memory-hacks-freezing-c-program-part-i.html

To build examples and test it run:
```
$ make

$ ./aslrdisable heap store
heap start: 0x602000
str allocated at 0x602010
saving 135168 bytes of heap
heap end: 0x623000
accessing dynamic allocated string: foo

$ ./aslrdisable heap load
heap start: 0x602000
loading 135168 bytes of heap
heap end: 0x623000
accessing dynamic allocated string: foo

$ ./aslrdisable stack store
initial stack at: 0x7fffffffe230
0
1
2
3
4
5
saving 64 bytes of stack
saving 936 bytes of register values
6
7
8
9
10

$ ./aslrdisable stack load
initial stack at: 0x7fffffffe230
stack at: 0x7fffffffe200
restored 64 bytes of stack from 0x7fffffffe230 to 0x7fffffffe1f0
register file loading done, ready to jump!
6
7
8
9
10
```
