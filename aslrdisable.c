#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> /* for fork */
#include <sys/types.h> /* for pid_t */
#include <sys/wait.h> /* for wait */
#define ADDR_NO_RANDOMIZE 0x0040000

int personality(int);

int main(int argc, char *argv[])
{
        pid_t pid;
        int personality_orig;
        /*Spawn a child to run the program.*/
        personality_orig = personality(0xffffffff);
        personality(personality_orig | ADDR_NO_RANDOMIZE);

        pid = vfork();

        if (pid == 0) { /* child process */
                char *argv_new[] = {argv[1], argv[2], NULL};
                execv(argv[1], argv_new);
                exit(127); /* only if execv fails */
        } else { /* pid != 0; parent process */
                waitpid(pid,0,0); /* wait for child to exit */
        }

        return 0;
}
